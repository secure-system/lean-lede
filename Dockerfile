FROM ubuntu:latest

# enforce non-interactive for tzdata etc
ENV DEBIAN_FRONTEND=noninteractive \
    # enforce mknod to allow root
    FORCE_UNSAFE_CONFIGURE=1

# # Prepare ccache
# RUN export CCACHE_DIR=$(echo staging_dir/toolchain-*)/ccache \
#     && export CONFIG_CCACHE=y

RUN apt-get -y update && apt-get -yq install --no-install-recommends python3.5 python2.7 \
    build-essential asciidoc binutils bzip2 swig rsync ccache \
    gawk gettext git libncurses5-dev libz-dev patch flex uglifyjs \
    unzip zlib1g-dev lib32gcc1 libc6-dev-i386 subversion \
    git-core gcc-multilib p7zip p7zip-full msmtp libssl-dev texinfo \
    libglib2.0-dev xmlto qemu-utils upx libelf-dev autoconf automake \
    libtool autopoint device-tree-compiler g++-multilib antlr3 gperf wget \
    && apt-get clean && rm -rf /var/lib/apt/lists/*

